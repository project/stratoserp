<?php

declare(strict_types=1);

namespace Drupal\stratoserp\Entity;

/**
 * Interface for base entities with item lines.
 */
interface StratosLinesEntityBaseInterface extends StratosEntityBaseInterface {

}
